This docker container includes:

- pip: aws-cli
- alpine packages: bash curl docker-compose git jq make python py-pip

Images:

- registry.gitlab.com/aifuzz/docker-ci-aws:packer

