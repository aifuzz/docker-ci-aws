#!/usr/bin/env bash

set -ex


chmod 400 "$SSH_PRIVATE_KEY"
ssh-add "$SSH_PRIVATE_KEY"
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo VALID_HOSTS = $VALID_HOSTS

ssh-keyscan $VALID_HOSTS >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts 

for host in $@
do
  echo HOST: $host
  ssh-keyscan $host >> ~/.ssh/known_hosts
done
exit

