#!/usr/bin/env bash
set -xe

environment="${ENVIRONMENT:-${CI_ENVIRONMENT_NAME:-unknown}}"
revision="${CI_COMMIT_SHA:-unknown}"
local_username="${GITLAB_USER_EMAIL:-unknown}"
project_name="${CI_PROJECT_NAME:-unknown}"

jo \
	environment=${environment} \
	revision=${revision} \
	local_username=${local_username} \
	comment="${project_name} deployed" \
	status=succeeded | \
curl --request POST \
 --url https://api.rollbar.com/api/1/deploy \
 --header "X-Rollbar-Access-Token: ${ROLLBAR_ACCESS_TOKEN}" \
 --header 'accept: application/json' \
 --header 'content-type: application/json' \
 --data @- | jq
