FROM ubuntu:22.04 AS build-ecr-login
COPY scripts /scripts
ENV DEBIAN_FRONTEND=noninteractive 
RUN : \
 && apt-get update \
 && apt-get install --no-install-recommends -y git golang-go ca-certificates amazon-ecr-credential-helper \
 && rm -rf /var/lib/apt/lists/* \
;
WORKDIR /build

FROM docker:20 as minimal
COPY scripts /scripts

RUN apk add --no-cache \
    alpine-sdk \
    bash \
    curl \
    docker-compose \
    docker-credential-ecr-login \
    git \
    jo \
    jq \
    make \
    nodejs \
    npm \
    openssh-client \
    python3 \
    py3-pip \
    skopeo \
    yarn

RUN pip install --no-cache-dir awscli

ENV PATH="/scripts:${PATH}"

FROM minimal as nodejs

FROM docker:20.10.21-dind-alpine3.16 as node-16
COPY scripts /scripts
RUN apk add --no-cache \
    bash \
    curl \
    docker-compose \
    git \
    jo \
    jq \
    make \
    nodejs \
    npm \
    openssh-client \
    python3 \
    py-pip \
    yarn


RUN pip install --no-cache-dir awscli

FROM docker:20.10.23-dind-alpine3.17 as node-18
COPY scripts /scripts
RUN apk add --no-cache \
    bash \
    curl \
    docker-compose \
    git \
    jo \
    jq \
    make \
    nodejs \
    npm \
    openssh-client \
    python3 \
    py-pip \
    yarn

RUN pip install --no-cache-dir awscli

FROM docker:25.0.3-dind-alpine3.19 as node-20
COPY scripts /scripts
RUN apk add --no-cache \
    bash \
    curl \
    docker-compose \
    git \
    jo \
    jq \
    make \
    nodejs \
    npm \
    openssh-client \
    python3 \
    py-pip \
    yarn

RUN pip install --no-cache-dir --break-system-packages awscli

FROM minimal as mobileapp

RUN apk add --no-cache \
    openjdk8-jre

RUN curl -sL https://github.com/rrx/java-play-store-uploader/releases/download/v1.0.7/PlayStoreUploader-v1.0.7.tar | tar x -f - -C /opt

ENV PATH ${PATH}:/opt/PlayStoreUploader-v1.0.7/bin

FROM gitlab/gitlab-runner:latest as ci-runner
COPY scripts /scripts

RUN apt update -q && apt install -qy \
    amazon-ecr-credential-helper \
    apksigner \
    docker.io \
    docker-compose \
    openssh-client \
    python3-pip \
    vim \
    zipalign \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install awscli

RUN base=https://github.com/docker/machine/releases/download/v0.16.0 && \
    curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && \
    mv /tmp/docker-machine /usr/local/bin/docker-machine && \
    chmod +x /usr/local/bin/docker-machine

FROM hashicorp/packer:light as packer
COPY scripts /scripts
RUN apk add --no-cache \
    bash \
    curl \
    docker \
    git \
    jo \
    jq \
    make \
    openssh-client \
    python3 \
    py3-pip

ENTRYPOINT "/bin/bash"


