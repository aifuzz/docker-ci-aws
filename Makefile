export CI_REGISTRY_IMAGE=registry.gitlab.com/aifuzz/docker-ci-aws
export CI_COMMIT_SHA=$(shell git rev-parse HEAD)
export DOCKER_BUILDKIT=1
CI_REGISTRY_IMAGE ?= docker-ci-aws

default:

build: build-minimal build-mobileapp build-nodejs build-node-16 build-node-18 build-runner build-packer

build-minimal:
	./build.sh minimal

build-mobileapp:
	./build.sh mobileapp

build-nodejs:
	./build.sh nodejs

build-node-16:
	./build.sh node-16

build-node-18:
	./build.sh node-18

build-runner:
	./build.sh ci-runner

build-packer:
	./build.sh packer

build-ubuntu-test:
	docker build --build-arg UBUNTU_BASE_VERSION=20.04 --tag $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA)-ubuntu-test --tag $(CI_REGISTRY_IMAGE):ubuntu-test ubuntu-test

