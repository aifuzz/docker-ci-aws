#!/usr/bin/env bash

set -x
TAG=$1
echo TAG:$TAG

docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA-${TAG}
docker push $CI_REGISTRY_IMAGE:${TAG}

