#!/usr/bin/env bash

set -x
TAG=$1
echo TAG:$TAG

docker build \
    --target=${TAG} \
    --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA-${TAG} \
    --tag $CI_REGISTRY_IMAGE:${TAG} src

